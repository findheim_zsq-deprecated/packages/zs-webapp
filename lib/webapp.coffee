cfg = require 'zs-app/lib/config'
env = require 'zs-app/lib/env'
winston = cfg.winston()
expressWinston = require 'express-winston'
express = require "express"
http = require "http"
path = require "path"
cookieParser = require "cookie-parser"
bodyParser = require "body-parser"
_ = require 'lodash'

class Webapp

  # init webapp
  init: ()->
    @_express = express()
    @_express.set 'port', cfg.get('port', 80)
    @_express.use bodyParser.json()
    # wire HTTP logging with winston - TODO: strange behavior, logs wrong path name
    unless cfg.get('disableExpressLogging',false)
      @_express.use expressWinston.logger { transports: _.values winston.transports }
    return this

  express: ()->
    return @_express

  server: ()->
    @_server

  start: (success, error)->
    unless @_express?
      throw new Error("Webapp has not been initialized.")

    # catch 404 and forwarding to error handler
    @_express.use (req, res, next) ->
      err = new Error("Page not found: #{req.path}")
      err.status = 404
      next err

    port = @_express.get 'port'

    @_server = http.createServer @_express
    @_server.on 'error', (err) =>
      winston.error "Failed to start zoomsquare webapp on port #{port}: #{err.message}"
      error? err
    @_server.listen port, (err) =>
      winston.info 'Web server listening on port ' + @_server.address().port
      success?()

  stop: (cb)->
    if @_server
      @_server.close () ->
        winston.info 'Server stopped.'
        cb()

  restart: ()->
    @stop @start

  static: (root, options)->
    express.static root, options
    
  newRouter: ()->
    express.Router()

module.exports = new Webapp().init()
