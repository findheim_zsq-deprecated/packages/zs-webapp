cfg = require 'zs-app/lib/config'
winston = cfg.winston()
Router = require '../restify/router'

try
  pkg = cfg.pkgInfo()
catch error
  winston.error 'Failed to get package.json info to display name of app via service zs-info.'

router = new Router()
router.get 'info', (req, res) ->
  res.end "#{pkg.name}, v#{pkg.version}"

module.exports = router
