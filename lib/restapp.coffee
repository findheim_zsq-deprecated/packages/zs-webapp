fs = require 'fs'
net = require 'net'

cfg = require 'zs-app/lib/config'
env = require 'zs-app/lib/env'
winston = cfg.winston()
restify = require 'restify'
_ = require 'lodash'

class RESTApp

  constructor: () ->
    @_port = cfg.get 'rest-port', 81
    @_host = cfg.get 'rest-host', '0.0.0.0'

  # init restify REST App
  # @param enableInfo enable info service under /info, default: enable unless production env
  init: (enableInfo) ->
    @_server = restify.createServer
      name: cfg.pkgInfo().name
      version: cfg.pkgInfo().version
      #log: bunyanToWinstonAdapter.createAdapter cfg.winston()
    @_server.use restify.gzipResponse()
    @_server.use restify.authorizationParser()
    @_server.use restify.bodyParser()
    @_server.use restify.queryParser()
    @_server.pre(restify.CORS())

    restify.CORS.ALLOW_HEADERS.push 'authorization'

    winston.info "CORS middleware registered."

    if enableInfo? and enableInfo or env.current != env.PRODUCTION
      require('./service/zs-info').register @_server
      winston.info "Info service enabled under /info"

    # this handles errors in routes
    # err is an error object/thingy from a domain. it has .stack and .message though
    unless global.zs_webapp_dont_handle_uncaught
      @_server.on "uncaughtException", (req, res, route, err) ->
        delete err.domain
        stack = err.stack?.split("\n")
        if stack.length > 1
          location = stack[1]
        else
          location = err.stack

        winston.error "uncaught exception in a restify route",
          message: err.message or 'unknown error in a route'
          source: location
          path: req.path()
          body: req.body

        res.json 500,
          status: "error"
          message: err.message or 'unknown error in a route'
        return false

    return this

  restify: () ->
    return @_server

  start: (success) ->
    unless @_server?
      throw new Error "REST app has not been initialized."
    @_server.listen @_port, =>
      # We ´use JSON.stringify because it can be an object, but also a string (when it is a unix socket)
      winston.info "REST server listening on #{JSON.stringify @_server.address()}"
      if success
        success()
    # Handle unix socket left over
    @_server.once 'error', (error) =>
      isUnixSocket = _.isString @_server.address()
      if isUnixSocket and error.code is 'EADDRINUSE'
        uconn = net.connect {path: @_server.address()}
        uconn.once 'error', (error) =>
          if error.code is 'ECONNREFUSED'
            winston.info 'Unix socket not in use anymore. Trying to delete'
            fs.unlinkSync @_port
            @_server.listen @_port
          else
            throw error
      else
        throw error

  stop: (success) ->
    unless @_server?
      throw new Error "REST app has not been initialized."
    @_server.close ->
      winston.info 'Server stopped.'
      if success
        success()

module.exports = new RESTApp().init()
