cfg = require 'zs-app/lib/config'
winston = cfg.winston()
_ = require 'lodash'

class Router

  constructor: () ->
    @_routes = {}

    methods = [ 'get', 'post', 'put','patch', 'del', 'head', 'all', 'options' ]
    _.forEach methods, (method) =>
      @_routes[method] = {}
      Router::[method] = (path, impl) ->
        routes = @_routes[method]
        routes[path] = impl

  register: (server, prefix='/') =>
    if server.restify?
      server = server.restify()
    unless prefix.match /^\//
      prefix = '/' + prefix
    for method, routes of @_routes
      for path, cb of routes
        p = prefix + path
        p = p.replace /\/\//g, '/'
        server[method] p, cb
        winston.debug "Registered route #{method.toUpperCase()} #{p}"

module.exports = Router