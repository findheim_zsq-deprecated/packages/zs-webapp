restapp = require '../lib/restapp'
cfg = require 'zs-app/lib/config'
_ = require 'lodash'
winston = cfg.winston()
restify = require 'restify'
assert = require 'assert'

port = cfg.get('rest-port')
if _.isString port
  client = restify.createClient {socketPath: port}
else
  client = restify.createClient {agent: false, url: "http://localhost:#{port}"}

# add mock test router
TEST_DATA = { test: 'TEST', num: 1234, nested: { ids: [ 1, 2, 3 ], foo: false } }
Router = require '../lib/restify/router'
router = new Router()
router.get 'restapp-init', (req, res) ->
  res.send TEST_DATA

# Test server init and test HTTP endpoints
describe 'zs-restapp', () ->

  before (done) ->
    router.register restapp.restify(), 'test/'
    restapp.start () ->
      done()

  after (done) ->
    console.log 'REST STOPPING'
    restapp.stop () ->
      console.log 'REST STOPPED'
      done()

  it 'should start the server', (done) ->
    done()
    #assert restapp._restify

  it 'should return test response', (done) ->
    client.get {path: "/test/restapp-init"}, (err, req) ->
      if err
        throw err
      req.on 'result', (error, res) ->
        if err
          throw err
        res.body = ''
        res.on 'data', (data) ->
          res.body += data
        res.on 'end', ->
          assert.deepEqual JSON.parse(res.body), TEST_DATA
          req.end()
          done()

  it 'should provide package info via service zs-info', (done) ->
    client.get {path: "/info"}, (err, req) ->
      if err
        throw err
      req.on 'result', (error, res) ->
        if err
          throw err
        res.body = ''
        res.on 'data', (data) ->
          res.body += data
        res.on 'end', ->
          pkg = cfg.pkgInfo()
          assert.equal res.body, "#{pkg.name}, v#{pkg.version}"
          req.end()
          done()