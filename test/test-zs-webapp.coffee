webapp = require '../lib/webapp'
cfg = require 'zs-app/lib/config'
fetch = require 'zs-app/lib/util/fetch'
assert = require 'assert'
express = require 'express'

port = 4129
app = webapp.express()

# add mock test router
router = express.Router()
router.get '/', (req, res) ->
  res.send 'TEST'
app.use '/test-webapp-init', router

# Test server init and test HTTP endpoints
describe 'zs-webapp', () ->

  before (done) ->
    webapp.start () ->
      done()

  after (done) ->
    webapp.stop () ->
      done()

  it 'should initialize express', () ->
    assert.equal (app.get 'port'), port

  it 'should start the server', () ->
    assert webapp._server

  it 'should return test response', (done) ->
    fetch "http://localhost:#{app.get 'port'}/test-webapp-init", (doc) ->
      assert.equal doc, 'TEST'
      done()
    , (err) ->
      assert false
      done()
