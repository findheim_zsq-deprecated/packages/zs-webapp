# zs-webapp - the module for Web apps & REST services

A module for express-based Web applications and restify-based REST services.

**Checkout [git@gitlab.zoomsquare.com:backend/realestate-service.git](https://gitlab.zoomsquare.com/backend/realestate-service) to see an example how this module can be used quickly.**

## Web apps and REST APIs

A web app can be either

- A non-REST web app with HTML views
- A REST service layer without HTML
- A REST-based web app with HTML views and a service layer

Because a web app with HTML views (GUI) has different requirements than a solid REST service layer, we use different libraries: express for GUIs, restify for the REST layer, both running on separate servers and ports.

## Using the module

Do a `npm install zs-webapp` or create a new module and add `zs-webapp` to `package.json`
   
  * Add the following line to your dependencies:
    
  		"zs-webapp": "git+ssh://git@gitlab.zoomsquare.com:packages/zs-webapp.git"
    	
  * Note that on Unix/Mac you can also `git clone git@gitlab.zoomsquare.com:packages/zs-webapp.git` and use [npm-link](https://www.npmjs.org/doc/cli/npm-link.html). This is useful when extending the base modules because by symlinking the files immediately change within `node_modules`.


## Web app quick guide

Use the webapp module for building web apps with rendered HTML views:

		cfg = require 'zs-app/lib/config'
        webapp = require 'zs-webapp/lib/webapp'
        app = webapp.express()
    
		# do what you do normally with express
		# app.use ...
		
        webapp.start()

		# you can stop the server as well:
		webapp.stop()

The `config` lib will init the config incl. winston logging as usual (see [zs-app](https://gitlab.zoomsquare.com/packages/zs-app) docs).

Read the [express](http://expressjs.com/) docs for further details!

## REST service quick guide

Use the restapp module for building REST services without HTML:

        restapp = require 'zs-webapp/lib/restapp'
        cfg = require 'zs-app/lib/config'
        
        # you can create multiple routers in order to bundle your endpoints into packages:
        require('./lib/service/legacy/estate').register restapp, 'estate/'
        require('./lib/service/legacy/score').register restapp, 'score/'
        require('./lib/service/legacy/stats').register restapp, 'stats/'        
        require('./lib/service/search/router').register restapp, 'search/'        
        restapp.start()

Where a router looks like this (e.g. `./lib/service/legacy/estate`):

        Router = require 'zs-webapp/lib/restify/router'
        router = new Router()
        router.post '/', (req, res) ->
          res.end 'Not implemented yet'

Read the [restify](http://mcavage.me/node-restify/) docs for further details!

### Config parameters:

* `port`: the port used by the server, default is 80
* `rest-port`: the REST API port used by the REST server, default is 81

## Authentication, SSL, Error handling, Compression, etc.

Everything supported by [restify](http://mcavage.me/node-restify/) out of the box or with plugins.

